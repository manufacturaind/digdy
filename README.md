# Digdy font

Digdy, a proportional pixel font with 3x5 px base.


![Font preview](https://gitlab.com/typebits/font-skeleton/-/jobs/artifacts/master/raw/Digdy-Regular.png?job=build-font)
  
[See it in action](https://manufacturaind.gitlab.io/digdy/)

Download formats:

* [TrueType (.ttf)](https://gitlab.com/manufacturaind/digdy/-/jobs/artifacts/master/raw/Digdy-Regular.ttf?job=build-font)
* [OpenType (.otf)](https://gitlab.com/manufacturaind/digdy/-/jobs/artifacts/master/raw/Digdy-Regular.otf?job=build-font)
* [FontForge (.sfd)](https://gitlab.com/manufacturaind/digdy/-/jobs/artifacts/master/raw/Digdy-Regular.sfd?job=build-font)

## Authors

[Manufactura Independente](https://manufacturaindependente.org)

## License

This font is made available under the [Open Font License](https://scripts.sil.org/OFL).
